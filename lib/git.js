var program = require("./program");
var spawn = require("child_process").spawn;
var byline = require("byline");
var Promise = require("bluebird");

module.exports = function(args, opts) {
    opts = opts || {};
    return new Promise(function(resolve, reject) {
        program.debug && console.log('spawning git ' + args.join(' ') + ' in ' + process.cwd());
        var gitProc = spawn('git', args, {
            cwd: opts.cwd || process.cwd(),
            stdio: (opts.stderr) ? ['pipe', 'pipe', process.stderr] : undefined,
            env: opts.env
        });
        var gitStream = byline(gitProc.stdout);
        gitStream.on('data', function(buff) {
            opts.onLine && opts.onLine(buff.toString());
        });
        gitProc.on('close', function() {
            opts.onClose && opts.onClose();
        });
        gitProc.on('exit', function (code) {
            program.debug && console.log('git ' + args.join(' ') + ' exited with ' + code);
            if (code) {
                reject('git ' + args.join(' ') + ' exited with ' + code);
            } else {
                resolve();
            }
        });
    });
};